# Non-extendable knight's paths

Exhaustive search for non-extendable knight's paths

See https://richholmes.xyz/topics/math/nep/ .

CC0 1.0 Richard Holmes https://creativecommons.org/publicdomain/zero/1.0/

Dependencies: Zelle graphics library
