"""
Exhaustive search for non-extendable knight's paths

See https://richholmes.xyz/topics/math/nep/ .

CC0 1.0 Richard Holmes https://creativecommons.org/publicdomain/zero/1.0/
"""

from sys import argv
from graphics import *    # Zelle graphics library
import argparse

size = 8  # Default size of (square) board

def compath (path1, path2):
    """
Compare two paths and return -1, 0, or 1 if the first is
"smaller than", equal to, or "bigger than" the second. Rather
arbitrarily, path1 < path2 if len(path1) < len(path2), or if, comparing
path1[0][0], path1[0][1], path1[1][0], path1[1][1]... to the corresponding
path2 entries, the first unequal entry is smaller.
"""
    
    if len(path1) < len(path2):
        ret = -1
    elif len(path1) > len(path2):
        ret = 1
    else:
        ret = 0
        for i in range (len(path1)):
            if path1[i][0] < path2[i][0]:
                ret = -1
                break
            if path1[i][0] > path2[i][0]:
                ret = 1
                break
            if path1[i][1] < path2[i][1]:
                ret = -1
                break
            if path1[i][1] > path2[i][1]:
                ret = 1
                break
    return ret

def mirror (path):
    """Mirror image of a path"""
    return [[size-1-s[0], s[1]] for s in path]

def rot (path):
    """Path rotated 90°"""
    return [[s[1], size-1-s[0]] for s in path]

def rev (path):
    """Path direction reversed"""
    return path[::-1]

def trans (path):
    """On a toroid, translate path to start at [0, 0]"""
    if path[0] == [0, 0]:
        return path
    ret = [[(p[0]-path[0][0])%size, (p[1]-path[0][1])%size] for p in path]
    return ret

def canon (path, toroid=False):
    """
Return canonical version of a path: The "smallest" (in the `compath` sense) path obtained by rotating, reflecting, and reversing the path. On a toroid, translate to start at [0,0]
"""

    s000 = list(path)
    s000m = mirror(s000)
    s000r = rev(s000)
    s000mr = rev(s000m)

    s090 = rot(s000)
    s090m = mirror(s090)
    s090r = rev(s090)
    s090mr = rev(s090m)

    s180 = rot(s090)
    s180m = mirror(s180)
    s180r = rev(s180)
    s180mr = rev(s180m)

    s270 = rot(s180)
    s270m = mirror(s270)
    s270r = rev(s270)
    s270mr = rev(s270m)

    ret = list(trans(s000) if toroid else s000)
    #print (f"canon {s000}->{ret}")
    for s in [s000m, s000r, s000mr, s090, s090m, s090r, s090mr,
              s180, s180m, s180r, s180mr, s270, s270m, s270r, s270mr]:
        #print (s, end="->")
        if toroid:
            s = trans(s)
        #print (s)
        if compath(s, ret) < 0:
            ret = list(s)
    #print (f"+++ {ret}")
    return ret

km = [[-2, -1], [-1, -2], [1, -2], [2, -1], [2, 1], [1, 2], [-1, 2], [-2, 1]]

def move (path, toroid=False, back=True):
    """
Given a path return a list of possible moves
Back (front) extensions if `back` == True (False)
If path is empty return list of valid initial positions.
If there are no possible moves return [].
"""
    ret = []
    idx = -1 if back else 0
    if path == []:
        if toroid:
            ret = [[0, 0]]
        else:
            for i in range((size+1)//2):
                for j in range (i, (size+1)//2):
                    ret.append([i, j])
    else:
        if toroid:
            ret = [[(path[idx][0]+kmi[0])%size, (path[idx][1]+kmi[1])%size] for kmi in km]
            ret = [r for r in ret if r not in path]
        else:
            ret = [[path[idx][0]+kmi[0], path[idx][1]+kmi[1]] for kmi in km]
            ret = [r for r in ret if r[0] >= 0 and r[0] < size and r[1] >= 0 and r[1] < size and r not in path]
                
    return ret

# Initial default value for maximum path length to check
mxl = 10

def findmin (path, toroid=False, minonly=True):
    """
Return a complete list of minimal length non-extendable paths starting with the input `path`.
"""
    global mxl

    ret = []
    ms = move(path, toroid)
    
    if ms == []:
        # There are no back moves, are there front moves?
        if move(path, toroid, False) == []:        
            # This is a minimal length non-extendable path, return its canonical form
            if minonly and len(path) < mxl:
                mxl = len(path)
            ret = [canon(path, toroid)]
    elif len(path) < mxl:
        # Path is extendable, check recursively
        for msi in ms:
            fm = findmin (path+[msi], toroid, minonly)
            for fmi in fm:
                if len(ret) == 0 or (minonly and len(fmi) < len(ret[0])):
                    ret = [fmi]
                elif not fmi in ret:
                    ret.append(fmi)

    return ret

def gpaths (paths, nx, ny, toroid=False, halve=False):
    """
Draw all paths on nx x ny board. Do not color squares if `toroid` and nx or ny is odd. If `halve`, halve last row/col.
"""
    size = 1000
    win = GraphWin("Non-extendable paths", size, size)
    ssq = 18   # square size
    scr = ssq/4  # circle radius
    lw = scr/2 # line width
    offx = ssq
    offy = ssq
    dnc = toroid and (nx%2==1 or ny%2==1)
    for path in paths:
        # Draw board
        col = 0
        for iy in range(ny):
            for ix in range(nx):
                if not halve or (ix < nx-1 and iy < ny-1):
                    r = Rectangle(Point(offx+ix*ssq, offy+iy*ssq), Point(offx+(ix+1)*ssq, offy+(iy+1)*ssq))
                    r.setFill("white" if col == 0 or dnc else "grey")
                    r.draw(win)
                elif ix == nx-1 and iy == ny-1:
                    r = Rectangle(Point(offx+ix*ssq, offy+iy*ssq), Point(offx+ix*ssq+ssq/2, offy+iy*ssq+ssq/2))
                    r.setFill("white" if col == 0 or dnc else "grey")
                    r.setWidth(0)
                    r.draw(win)
                    l = Line(Point(offx+ix*ssq, offy+iy*ssq), Point(offx+ix*ssq+ssq/2, offy+iy*ssq))
                    l.draw(win)
                    l = Line(Point(offx+ix*ssq, offy+iy*ssq), Point(offx+ix*ssq, offy+iy*ssq+ssq/2))
                    l.draw(win)
                elif ix == nx-1:
                    r = Rectangle(Point(offx+ix*ssq, offy+iy*ssq), Point(offx+ix*ssq+ssq/2, offy+iy*ssq+ssq))
                    r.setFill("white" if col == 0 else "grey")
                    r.setWidth(0)
                    r.draw(win)
                    l = Line(Point(offx+ix*ssq, offy+iy*ssq), Point(offx+ix*ssq+ssq/2, offy+iy*ssq))
                    l.draw(win)
                    l = Line(Point(offx+ix*ssq, offy+iy*ssq), Point(offx+ix*ssq, offy+iy*ssq+ssq))
                    l.draw(win)
                else:
                    r = Rectangle(Point(offx+ix*ssq, offy+iy*ssq), Point(offx+ix*ssq+ssq, offy+iy*ssq+ssq/2))
                    r.setFill("white" if col == 0 else "grey")
                    r.setWidth(0)
                    r.draw(win)
                    l = Line(Point(offx+ix*ssq, offy+iy*ssq), Point(offx+ix*ssq+ssq, offy+iy*ssq))
                    l.draw(win)
                    l = Line(Point(offx+ix*ssq, offy+iy*ssq), Point(offx+ix*ssq, offy+iy*ssq+ssq/2))
                    l.draw(win)
                col = 1-col
            if nx%2 == 0:
                col = 1-col
        ocp = None
        for s in path:
            cp = Point(offx + ssq*(s[0] + 0.5), offy + ssq*(s[1] + 0.5))
            c = Circle(cp, scr)
            c.setFill ("black")
            c.draw(win)
            if ocp != None:
                l = Line (ocp, cp)
                l.setWidth(lw)
                l.setOutline("red")
                l.draw(win)
            ocp = cp
            
        offx += (nx+1)*ssq
        if offx > size-(nx+1)*ssq:
            offx = ssq
            offy += (ny+1)*ssq
            if offy > size-(ny+1)*ssq:
                win.getMouse()
                
                win.close()
                win = GraphWin("Non-extendable paths", size, size)
                offy  = ssq
                
    win.getMouse()
    win.close()
                
    
def main():
    global size
    global mxl
    global km
    
    """
Optional positional args are:
* size of square board (integer, default 8)
* initial value for minimum length (integer, default 10)
"""
    parser = argparse.ArgumentParser()

    parser.add_argument (
        'size',
        type=int,
        nargs='?',
        default = 8,
        help="Size of (square) board, default 8"
    )

    parser.add_argument (
        '-mxl',
        type=int,
        default = 10,
        help="(Initial) maximum length to check, default 10"
    )

    parser.add_argument (
        '-a',
        action="store_true",
        help="Save all length solutions to maximum; save only min length if not specified"
        )
    
    parser.add_argument (
        '-g',
        action="store_true",
        help="Draw solutions"
    )

    parser.add_argument (
        '-gh',
        action="store_true",
        help="Draw solutions with last row/column halved"
    )

    parser.add_argument (
        '-t',
        action="store_true",
        help="Solve on torus"
    )

    parser.add_argument (
        '-v',
        type=int,
        default=1,
        help="Verbosity: Summary only if 0, summary+solutions if >0"
        )
    
    args = parser.parse_args()
    
    size = args.size
    mxl = args.mxl+1   # +1 for positions not moves
    minonly = not args.a
    graphics = args.g or args.gh
    halve = args.gh
    toroid = args.t
    verb = args.v
    
    if toroid:
        kkm = []
        for k in km:
            kk = [ki%size for ki in k]
            if kk not in kkm:
                kkm.append(kk)
        km = list(kkm)
    
    paths = findmin ([], toroid, minonly)

    if len(paths) > 0:
        lpa = [len(p) for p in paths]
        mnlp = min(lpa)
        mxlp = max(lpa)
        print (f"Minimum path length: {mnlp-1} moves")
        print (f"Total {len(paths)} solutions")
        pss = []
        for lp in range (mnlp, mxlp+1):
            ps = [p for p in paths if len(p)==lp]
            pss += ps
            if ps != []:
                if mnlp < mxlp:
                    print (f"{lp-1} moves, {len(ps)} solutions{':' if verb>0 else ''}")
                if verb > 0:
                    for si in ps:
                        print (si)
        if graphics:
            gpaths(ps, size, size, toroid, halve)
    else:
        print (f"No paths found with length <= {mxl}")

if __name__ == "__main__":
    main()
